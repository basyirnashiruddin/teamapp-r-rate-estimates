export const employees = [
  {
    active: true,
    address: "Jl. Tebet no 5",
    division: {
        active: true,
        id: 2,
        name: "Data Orchestration"
    },
    email: "panji.asmoro@metrocom.co.id",
    id: 2,
    job: {
        active: true,
        id: 1,
        title: "Web Design"
    },
    joinDate: "2010-05-05Z",
    name: "Panji",
    avatarUrl: '/static/images/avatars/avatar_3.png',
    roles: {
        active: true,
        edited: "2021-11-22T05:24:05Z[UTC]",
        billCode: "600",
        id: 1,
        rate: 30000000.00,
        role: "Web Designer"
    }
  }
];
